package com.example.user.xox;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int mSize = 4;
    private Button[][] buttons = new Button[mSize][mSize];

    private boolean player1Turn = true;

    private int roundCount;

    private int player1Points;
    private int player2Points;

    private TextView textViewPlayer1;
    private TextView textViewPlayer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        textViewPlayer1 = findViewById(R.id.text_view_p1);
        textViewPlayer2 = findViewById(R.id.text_view_p2);

        for (int i = 0; i < mSize; i++) {
            for (int j = 0; j < mSize; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
                buttons[i][j].setTag(String.format("%s,%s", i, j));
            }
        }

        Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (!((Button) v).getText().toString().equals("")) {
            return;
        }

        if (player1Turn) {
            ((Button) v).setText("X");
        } else {
            ((Button) v).setText("O");
        }

        roundCount++;

        String[] tag = ((String) v.getTag()).split(",");
        if (tag.length != 2) {
            Log.e("debug", "Not tag contains. TAG: " + v.getTag());
            return;
        }

        if (checkForWin(Integer.parseInt(tag[0]), Integer.parseInt(tag[1]))) {
            if (player1Turn) {
                player1Wins();
            } else {
                player2Wins();
            }
        } else if (roundCount == mSize * mSize) {
            draw();
        } else {
            player1Turn = !player1Turn;
        }

    }

    private boolean checkForWin(int x, int y) {
        String[][] field = new String[mSize][mSize];
        for (int i = 0; i < mSize; i++)
            for (int j = 0; j < mSize; j++)
                field[i][j] = buttons[i][j].getText().toString();

        String select = field[x][y];
        Log.i("debug", String.format("Check win (%s, %s) => %s", x, y, select));

        // ------------------------------merkez kontrolü--------------------------------------------

        // dikey kontrol
        try {
            if (field[x][y + 1].equals(select) && field[x][y - 1].equals(select) && !select.equals("")) {
                buttons[x][y + 1].setBackgroundColor(Color.RED);
                buttons[x][y - 1].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // yatay kontrol
        try {
            if (field[x + 1][y].equals(select) && field[x - 1][y].equals(select) && !select.equals("")) {
                buttons[x + 1][y].setBackgroundColor(Color.RED);
                buttons[x - 1][y].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sağ çapraz konrol
        try {
            if (field[x + 1][y + 1].equals(select) && field[x - 1][y - 1].equals(select) && !select.equals("")) {
                buttons[x + 1][y + 1].setBackgroundColor(Color.RED);
                buttons[x - 1][y - 1].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sol çapraz kontrol
        try {
            if (field[x + 1][y - 1].equals(select) && field[x - 1][y + 1].equals(select) && !select.equals("")) {
                buttons[x + 1][y - 1].setBackgroundColor(Color.RED);
                buttons[x - 1][y + 1].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        // ------------------------------uç uca kontrolü---------------------------------------------

        // dikey yukarı kontrol
        try {
            if (field[x][y + 1].equals(select) && field[x][y + 2].equals(select) && !select.equals("")) {
                buttons[x][y + 1].setBackgroundColor(Color.RED);
                buttons[x][y + 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // dikey aşağı kontrol
        try {
            if (field[x][y - 1].equals(select) && field[x][y - 2].equals(select) && !select.equals("")) {
                buttons[x][y - 1].setBackgroundColor(Color.RED);
                buttons[x][y - 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // yatay sağ kontrol
        try {
            if (field[x + 1][y].equals(select) && field[x + 2][y].equals(select) && !select.equals("")) {
                buttons[x + 1][y].setBackgroundColor(Color.RED);
                buttons[x + 2][y].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // yatay sol kontrol
        try {
            if (field[x - 1][y].equals(select) && field[x - 2][y].equals(select) && !select.equals("")) {
                buttons[x - 1][y].setBackgroundColor(Color.RED);
                buttons[x - 2][y].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sağ çapraz yukarı konrol
        try {
            if (field[x + 1][y + 1].equals(select) && field[x + 2][y + 2].equals(select) && !select.equals("")) {
                buttons[x + 1][y + 1].setBackgroundColor(Color.RED);
                buttons[x + 2][y + 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sağ çapraz aşağı konrol
        try {
            if (field[x - 1][y - 1].equals(select) && field[x - 2][y - 2].equals(select) && !select.equals("")) {
                buttons[x - 1][y - 1].setBackgroundColor(Color.RED);
                buttons[x - 2][y - 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sol çapraz yukarı kontrol
        try {
            if (field[x + 1][y - 1].equals(select) && field[x + 2][y - 2].equals(select) && !select.equals("")) {
                buttons[x + 1][y - 1].setBackgroundColor(Color.RED);
                buttons[x + 2][y - 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // sol çapraz yukarı kontrol
        try {
            if (field[x - 1][y + 1].equals(select) && field[x - 2][y + 2].equals(select) && !select.equals("")) {
                buttons[x - 1][y + 1].setBackgroundColor(Color.RED);
                buttons[x - 2][y + 2].setBackgroundColor(Color.RED);
                buttons[x][y].setBackgroundColor(Color.RED);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void player1Wins() {
        player1Points++;
        Toast.makeText(this, "Player 1 wins!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void player2Wins() {
        player2Points++;
        Toast.makeText(this, "Player 2 wins!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void draw() {
        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updatePointsText() {
        textViewPlayer1.setText("Player 1: " + player1Points);
        textViewPlayer2.setText("Player 2: " + player2Points);
    }

    private void resetBoard() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);

                    for (int i = 0; i < mSize; i++)
                        for (int j = 0; j < mSize; j++) {
                            buttons[i][j].setText("");
                            buttons[i][j].setBackgroundColor(Color.WHITE);
                        }

                    roundCount = 0;
                    player1Turn = true;

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void resetGame() {
        player1Points = 0;
        player2Points = 0;
        updatePointsText();
        resetBoard();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("roundCount", roundCount);
        outState.putInt("player1Points", player1Points);
        outState.putInt("player2Points", player2Points);
        outState.putBoolean("player1Turn", player1Turn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        roundCount = savedInstanceState.getInt("roundCount");
        player1Points = savedInstanceState.getInt("player1Points");
        player2Points = savedInstanceState.getInt("player2Points");
        player1Turn = savedInstanceState.getBoolean("player1Turn");
    }
}

