package com.example.user.xox;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class LoginActivity extends AppCompatActivity {

    EditText et_email, et_sifre;
    Button b_giris, b_yeni_kullanici;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_email = (EditText) findViewById(R.id.et_email);
        et_sifre = (EditText) findViewById(R.id.et_sifre);
        b_giris = (Button)findViewById(R.id.b_giris);
        b_yeni_kullanici = (Button)findViewById(R.id.b_yeni_kullanici);

        b_giris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this , StartActivity.class);
                startActivity(intent);}
        });


        b_yeni_kullanici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, CreateNewUser.class);
                startActivity(intent1);
            }
        });
    }
    }

