package com.example.user.xox;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class CreateNewUser extends AppCompatActivity {

    EditText et_yeni_email, et_yeni_sifre;
    Button b_yeni_kullanici, b_test;


    Activity activity = this;
    Button btnTest;
    ListView listView;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_user);

        dbHelper = new DBHelper(activity);

        et_yeni_email = (EditText) findViewById(R.id.et_yeni_email);
        et_yeni_sifre = (EditText) findViewById(R.id.et_yeni_sifre);

        listView = (ListView) findViewById(R.id.new_user_list);
        btnTest = (Button) findViewById(R.id.b_test);
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SQLiteDatabase db = veritabani.getWritableDatabase();

//                List<String> veriler = new ArrayList<String>();
//                Cursor cursor = db.query(veritabani.KISILER_TABLE, new String[]{veritabani.ROW_ID, veritabani.ROW_MAIL, veritabani.ROW_SIFRE}, null, null, null, null);
//
//                while (cursor.moveToNext()) {
//                    veriler.add(cursor.getInt(0) + "--" + cursor.getInt(1) + cursor.getInt(2));
//                }
                ArrayList<String> list = new ArrayList<>();
                for (DBHelper.UserModel u : dbHelper.listUserModels())
                    list.add(u.toString());

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        CreateNewUser.this,
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        list
                );
                listView.setAdapter(adapter);
            }
        });



/*

        b_test = (Button)findViewById(R.id.b_test);
        ListView = (ListView)findViewById(R.id.ListView);
        b_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Veritabani veritabani = new Veritabani(CreateNewUser.this);
                SQLiteDatabase db = veritabani.getWritableDatabase() ;
                List<String> veriler = new ArrayList<String> ();
                Cursor cursor = db.query(veritabani.KISILER_TABLE, new String[] {veritabani.ROW_ID, veritabani.ROW_MAIL, veritabani.ROW_SIFRE}, null, null, null, null );

                while (cursor.moveToNext()) {
                    veriler.add(cursor.getInt(0)+ "--" + cursor.getInt(1)+ cursor.getInt(2));
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String> (CreateNewUser.this , android.R.layout.simple_list_item_1, android.R.id.text1, veriler);
                ListView.setAdapter(adapter);
            }
        });

*/


        b_yeni_kullanici = (Button) findViewById(R.id.b_yeni_kullanici);
        b_yeni_kullanici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Veritabani veritabani = new Veritabani(CreateNewUser.this);
//                veritabani.VeriEkle(et_yeni_email.getText().toString(), et_yeni_sifre.getText().toString());
//
//
//                Intent intent2 = new Intent(CreateNewUser.this, StartActivity.class);
//                startActivity(intent2);

                dbHelper.addUser(new DBHelper.UserModel("1", "a", "a", "a"));
            }
        });
    }
}




