package com.example.user.xox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class StartActivity extends AppCompatActivity {

    Button b_basla;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);


        imageView = (ImageView)findViewById(R.id.imageView);
        b_basla = (Button)findViewById(R.id.b_basla);
        b_basla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent3);
            }
        });
    }
}
