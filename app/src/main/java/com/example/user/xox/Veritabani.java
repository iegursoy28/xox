package com.example.user.xox;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Veritabani extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "Veritabani";
    private static final int DATABASE_VERSION =1;
    private static final String KISILER_TABLE  = "kisiler";

     public static final String ROW_ID ="id";
     public static final String ROW_MAIL ="email ";
     public static final String ROW_SIFRE = "sifre";


    public Veritabani(Context context) {
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + KISILER_TABLE + "("+ROW_ID+"INTEGER PRIMARY KEY, "+ROW_MAIL+" TEXT NOT NULL , "+ROW_SIFRE+" TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " +KISILER_TABLE);
        onCreate(db);
    }

    public void  VeriEkle(String mail, String sifre) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROW_MAIL, mail.trim());
        cv.put(ROW_SIFRE, sifre.trim());
        db.insert(KISILER_TABLE, null, cv);
        db.close();
    }
}
